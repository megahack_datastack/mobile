import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Routes } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WhoAreYouComponent } from './who-are-you/who-are-you.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { SearchContracts } from './dashboard/pipes/filter-contracts';
import { NewContractComponent } from './new-contract/new-contract.component';
import { ContractDetailsComponent } from './contract-details/contract-details.component';
import { OcrServiceProvider } from './providers/ocr/ocr-service';
import { ClientService } from './providers/client.service';
import { LoginService } from './providers/login.service';
import { ApiService } from './providers/api.service';
import { ToastsService } from './providers/toasts.service';
import { AuthGuard } from './providers/auth-guard.service';
import { LoginAuthGuard } from './providers/login-auth-guard';
import { HasRegistrationGuard } from './providers/has-registration-guard.service';
import { Base64 } from "@ionic-native/base64/ngx";
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { SignatureComponent } from './signature/signature.component';
import { SignaturePadModule } from "angular2-signaturepad";

import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';

@NgModule({
  declarations: [AppComponent, LogInComponent, SignUpComponent, WhoAreYouComponent, DashboardComponent, SearchContracts, NewContractComponent, ContractDetailsComponent, SignatureComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, FormsModule, SignaturePadModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ToastsService,
    OcrServiceProvider,
    ClientService,
    LoginService,
    ApiService,
    AuthGuard,
    LoginAuthGuard,
    HasRegistrationGuard,
    Base64,
    BarcodeScanner,
    Camera,
    File, FileTransfer, DocumentViewer
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
