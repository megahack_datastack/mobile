import { Injectable }       from '@angular/core';
import {
  Router,
  NavigationExtras, ActivatedRouteSnapshot, RouterStateSnapshot,
  CanActivate, Route
}                           from '@angular/router';
import { LoginService } from './login.service';
import { ClientService } from './client.service';

@Injectable()
export class LoginAuthGuard implements CanActivate {
  constructor(private client: ClientService, private loginService: LoginService, private router: Router) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (!this.client.isAuthorized()) {
        return true; 
    }

    this.router.navigate(['/dashboard']);
    return false;
  }
}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/