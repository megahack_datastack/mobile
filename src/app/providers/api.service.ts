import { Injectable } from "@angular/core";
import gql from "graphql-tag";
import { ClientService } from "./client.service";

@Injectable()
export class ApiService {
  public static fragments = {

  };

  constructor(public client: ClientService) { }

  public async me(): Promise<any> {
    let data;
    try {
      let result = await this.client.scope(executor => executor.query(gql`
        query {
          me { email, hasCompletedRegistration }
        }
      `));
      data = result.data;
      if(!data) return false;
    } catch(ex) {
      console.error(ex);
    }
    return data || false;
  }

  public async document(dt: any): Promise<any> {
    let data;
    try {
      let result = await this.client.scope(executor => executor.mutate(gql`
        mutation (
          $documentType: String
          $documentNumber: String
          $surNames: String
          $givenNames: String
          $countryCode: String
          $issuingCountryCode: String
          $nationalityCountryCode: String
          $dayOfBirth: String
          $expirationDate: String
          $sex: String
          $personalNumber: String
          $checkDigitNumber: String
          $checkDigitPersonalNumber: String
          $checkDigitDayOfBirth: String
          $checkDigitExpirationDate: String
          $checkDigitFinal: String
          $allCheckDigitsValid: String
          $dayOfBirthObject: String
          $expirationDateObject: String
          $mrzString: String
          $uploaded: String
        ) {
          document(
            documentType: $documentType
            documentNumber: $documentNumber
            surNames: $surNames
            givenNames: $givenNames
            countryCode: $countryCode
            issuingCountryCode: $issuingCountryCode
            nationalityCountryCode: $nationalityCountryCode
            dayOfBirth: $dayOfBirth
            expirationDate: $expirationDate
            sex: $sex
            personalNumber: $personalNumber
            checkDigitNumber: $checkDigitNumber
            checkDigitPersonalNumber: $checkDigitPersonalNumber
            checkDigitDayOfBirth: $checkDigitDayOfBirth
            checkDigitExpirationDate: $checkDigitExpirationDate
            checkDigitFinal: $checkDigitFinal
            allCheckDigitsValid: $allCheckDigitsValid
            dayOfBirthObject: $dayOfBirthObject
            expirationDateObject: $expirationDateObject
            mrzString: $mrzString
            uploaded: $uploaded
          ) { id }
        }      
      `, { variables: dt }));
      data = result.data;
      if(!data) return false;
    } catch(ex) {
      return false;
    }
    return data || false;
  }

  public async contract(id: string): Promise<any> {
    let data;
    try {
      let result = await this.client.scope(executor => executor.query(gql`
        query {  
          signedContract {
            contract { id, name, description, preview, uploaded, provider { name } },
            signature { id },
            qr
          }
        }
      `), context => context.resource('contract', id));
      data = result.data;
      if(!data) return false;
    } catch(ex) {
      return false;
    }
    return data || false;
  }

  public async authorize(uploaded: string): Promise<any> {
    let data;
    try {
      let result = await this.client.scope(executor => executor.mutate(gql`
        mutation ($uploaded: String!) {
          authorize(uploaded: $uploaded) { id, authorized, user }
        }
      `, { variables: { uploaded }}));
      data = result.data;
      if(!data) return false;
    } catch(ex) {
      return false;
    }
    return data ? data.authorize : false;
  }

  public async sign(contract: string, authorization: string, uploaded: string): Promise<any> {
    let data;
    try {
      let result = await this.client.scope(executor => executor.mutate(gql`
        mutation($uploaded: String!, $authorization: String!, $contract: String!) {
          sign(uploaded: $uploaded, authorization: $authorization, contract: $contract) {
            id
          }
        }
      `, { variables: { uploaded, contract, authorization }}));
      data = result.data;
      if(!data) return false;
    } catch(ex) {
      return false;
    }
    return data.sign || false;
  }

  public async contracts(): Promise<any[]> {
     let data;
     try {
      let result = await this.client.scope(executor => executor.query(gql`
        query {
          me {
            contracts { id, name, name, provider { name } }
          }
        }
      `))
      data = result.data;
      if(!data || !data.me) return [];
     } catch(ex) {
       return [];
     }

    return data.me.contracts || [];
  }
}
