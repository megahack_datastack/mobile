import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import * as $ from 'jquery';

const toastDefaultOptions = {
  dismissOnClick: true,
  dismissOnTimeout: true,
  timeout: 6000,
  screenBlock: false
};

function Timer(callback, delay) {
  let timerId;
  let start;
  let remaining: any = delay;
  this.pause = function () {
      let now : any = new Date();
      window.clearTimeout(timerId);
      remaining -= now - start;
  };
  this.resume = function () {
      start = new Date();
      window.clearTimeout(timerId);
      timerId = window.setTimeout(callback, remaining);
  };
  this.extend = function () {
      window.clearTimeout(timerId);
      remaining = delay;
      timerId = window.setTimeout(callback, remaining);
  };
  this.resume();
}

let timers = {};
let id = 0;

function toastTemplate(type, content) {
  let template = "";
  template += "<div class='toast new " + type + "'><div class='container'><div class='content'><p>" + content + "</p></div></div></div>";
  return template;
}

function dismissToast(toast) {
  toast.addClass('fadeOut');
  setTimeout(function () {
      delete timers[toast.attr('id')];
      toast.remove();
  }, 700);
}

function dismissAll() {
  $('#toasts-container').html('');
}

function unblockScreen(screenBlock) {
  if (screenBlock) {
      let toastsContainer = $('#toasts-container');
      toastsContainer.removeClass("blockScreen");
      dismissAll();
  }
}

function setToastOptions(type, options, callback) {
  let toast = $('#toasts-container .toast.new'),
      toastsContainer = $('#toasts-container');
  if (typeof callback === "function" && options.dismissOnClick) {
      toast.on('click', function () {
          callback();
          unblockScreen(options.screenBlock);
          dismissToast(toast);
      });
  } else if (typeof callback === "function" && !options.dismissOnClick) {
      toast.on('click', function () {
          callback();
          unblockScreen(options.screenBlock);
      });
  } else if ((typeof callback !== "function") && options.dismissOnClick) {
      toast.on('click', function () {
          dismissToast(toast);
          unblockScreen(options.screenBlock);
      });
  }

  if (options.screenBlock) {
      toastsContainer.addClass("blockScreen");

  } else {
      if (options.dismissOnTimeout) {
          toast.attr('id', type + id);
          id++;
          timers[toast.attr('id')] = new Timer(function () {
              dismissToast(toast);
          }, options.timeout);
          toast.on('mouseenter', function () {
              timers[toast.attr('id')].pause();
          });
          toast.on('mouseleave', function () {
              timers[toast.attr('id')].resume();
          });
      }
  }

  toast.removeClass('new');
}

function duplicate(type, content) {
  let existingToasts = $(".toast." + type);

  for (let i = 0; i < existingToasts.length; i++) {
      if ($(existingToasts[i]).find(".content p")[0].innerHTML === content) {
          if (existingToasts[i].id !== '') {
              timers[existingToasts[i].id].extend();
          }
          return true;
      }
  }
  return false;
}

@Injectable()
export class ToastsService {

  constructor() { }

  createToast = function (type, content, options: any = {}, callback: any = false) {
    if (duplicate(type, content)) {
        return;
    } else {
        if (typeof options === 'function') {
            callback = options;
            options = {};
        }
        if (!options) {
            options = {};
        }
        _.defaults(options, toastDefaultOptions);
        var toast = toastTemplate(type, content);
        if (options.screenBlock) {
           dismissAll();
        }
        $("#toasts-container").append(toast);
        setToastOptions(type, options, callback);
    }
  }

  dismissToasts = function () {
      this.dismissAll();
  }
}
