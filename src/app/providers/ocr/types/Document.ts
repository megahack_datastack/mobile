import { Outline } from "./Outline";

export interface Document {
  confidence: number;
  outline: Outline;

  documentType: string;
  documentNumber: string;
  surNames: string;
  givenNames: string;
  countryCode: string;
  issuingCountryCode: string;
  nationalityCountryCode: string;
  dayOfBirth: string;
  expirationDate: string;
  sex: string;
  personalNumber: string;
  checkDigitNumber: string;
  checkDigitPersonalNumber: string;
  checkDigitDayOfBirth: string;
  checkDigitExpirationDate: string;
  checkDigitFinal: string;
  allCheckDigitsValid: string;
  dayOfBirthObject: string;
  expirationDateObject: string;
  mrzString: string;
  imagePath: string;
};
