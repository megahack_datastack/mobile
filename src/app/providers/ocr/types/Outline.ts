import { Point } from './Point';
export interface Outline {
    upRight: Point;
    upLeft: Point;
    downRight: Point;
    downLeft: Point;
};