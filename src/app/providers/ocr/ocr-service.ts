import { Injectable } from "@angular/core";
import { Document } from './types/Document';

@Injectable()
export class OcrServiceProvider {
  private static license: string = "eyJzY29wZSI6WyJBTEwiXSwicGxhdGZvcm0iOlsiaU9TIiwiQW5kcm9pZCIsIldpbmRvd3MiXSwidmFsaWQiOiIyMDE4LTEwLTIyIiwibWFqb3JWZXJzaW9uIjoiMyIsImlzQ29tbWVyY2lhbCI6ZmFsc2UsInRvbGVyYW5jZURheXMiOjMwLCJzaG93UG9wVXBBZnRlckV4cGlyeSI6dHJ1ZSwiaW9zSWRlbnRpZmllciI6WyJsYWJzLmRhdGFzdGFjay5tZWdhaGFjayJdLCJhbmRyb2lkSWRlbnRpZmllciI6WyJsYWJzLmRhdGFzdGFjay5tZWdhaGFjayJdLCJ3aW5kb3dzSWRlbnRpZmllciI6WyJsYWJzLmRhdGFzdGFjay5tZWdhaGFjayJdfQpTY0hBd3ZnSytuY3RKNXZYVEtESVNVc2NsUlltb0lncmlzQ2d5dTE5aVZ2aURMQ25JcGpwM2J5aWQ4OHJLM0JuU0tET3hvUk9iRXVhWnM2OUQ1ODRBclFpRHNieC8zLzhFZk1VdXlsWEdmclF5Q2QwMzBYNUJUNjgyQWlZbWs5U242VTE2VjJwU3dOU010Q1UxZFVqQ2lwTllQZnU0Q3NsSlp3RDAvd0Q2ejk2VVZpb1NUeGNUejVYUjhIQWMzSWhFa2UyVlJIZ2gyVmd0RHhGUFFmNUlMQWlVZytzQXNDSEpxa2Ixa252cnJ2bGhYVjBSdXJNdzZEMDRuSkNDMmtoNEw5OWwxRzc0MkxoeDRJZUEvdmJEOFlLZkN1NVR4S0VnUVlON0E5aVJkSElLOU9jVllscGpQZTQ3ZGUrNnhrMERhS2ZQSHMxVDB1eTFpQW1GTS9UVEE9PQ==";

  public process(input: any): Document {
    input.outline = JSON.parse(input.outline);
    delete input.personalNumber2;
    return input;
  };

  public scan(): Promise<any> {
    return new Promise((resolve, reject) => {
      (<any>window).cordova.exec(resolve, reject, "AnylineSDK", "MRZ", [
        OcrServiceProvider.license,
        {
          captureResolution: "1080",
          cutout: {
            style: "rect",
            maxWidthPercent: "80%",
            maxHeightPercent: "80%",
            alignment: "top_half",
            width: 870,
            ratioFromSize: {
              width: 5,
              height: 1
            },
            strokeWidth: 2,
            cornerRadius: 10,
            strokeColor: "FFFFFF",
            outerColor: "000000",
            outerAlpha: 0.3
          },
          flash: {
            mode: "manual",
            alignment: "bottom_right"
          },
          beepOnResult: true,
          vibrateOnResult: true,
          blinkAnimationOnResult: true,
          cancelOnResult: true
        },
        {
          scanMode: "LINE",
          minCharHeight: 30,
          maxCharHeight: 60,
          charWhitelist: "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
          validationRegex: "^[A-Z]{2}([0-9A-Z]\\s*){13,32}$",
          minConfidence: 65,
          removeSmallContours: true,
          charCountX: 4,
          charCountY: 2,
          charPaddingXFactor: 1.0,
          charPaddingYFactor: 1.0,
          isBrightTextOnDark: false,
          drawTextOutline: true
        }
      ]);
    });
  }
}
