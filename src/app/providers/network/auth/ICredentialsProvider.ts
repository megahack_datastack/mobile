import { Credentials } from './Credentials';

/**
 * Class responsible to provide and handle the credentials
 */
export interface ICredentialsProvider {
    /**
     * Method used to provide the credentials from the store
     * @param if true, will force the renewal of the credentials
     * @returns Valid set of credentials or null
     */
    export(force: boolean): Credentials | null;

    /**
     * Method used to save the current set of credentials to the store
     * @param credentials Set of credentials that will be stored
     * @returns false is the save didn't succeded, true otherwise
     */
    save(credentials: Credentials): boolean;

    /**
     * Method used to revoke the current set of credentials.
     * @returns false is the revoke process failed, true otherwise
     */
    revoke(): boolean;
};