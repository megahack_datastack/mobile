/**
 * Container class for the tokens
 */
export interface AuthTokens {
    access: string;
    refresh: string;
};