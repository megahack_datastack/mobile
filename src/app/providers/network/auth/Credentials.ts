import { AuthTokens } from './AuthTokens';

/**
 * Class used as a container for the tokens. Also compiles/decompiles them into/from a string format. 
 */
export class Credentials {
    public tokens: AuthTokens;

    public constructor(private compiled: string) {
        this.decompile(this.compiled);
    };

    /**
     * Decompiles a string containing the compiled credentials and stores them into this object
     * @param compiled The compiled credentials
     * @returns false if the compiled credentials have an invalid format, true otherwise
     */
    public decompile(compiled: string): boolean {
        
        if(!compiled) return false;
        let assemblyComponents = compiled.split('|');
        if(assemblyComponents.length != 2) return false;

        this.compiled = compiled;
        this.tokens = { access: assemblyComponents[0], refresh: assemblyComponents[1] };
        
        return true;
    };

    /**
     * Compiles the credentials into a single string
     * @param force Forces the recompilation [default is false]
     * @returns The result of the compilation
     */
    public compile(force: boolean = false): string {
        if(!this.compiled || force) {
            this.compiled = `${this.tokens.access}|${this.tokens.refresh}`;
        }
        
        return this.compiled;
    };
};