import { ICredentialsProvider } from './ICredentialsProvider';
import { Credentials } from './Credentials';

export class LocalStorageCredentialsProvider implements ICredentialsProvider {
    private cached: Credentials = null;

    public constructor(private key: string) { };

    public export(force: boolean = true): Credentials | null {
        if(!this.cached || force) {
            let compiledTokens = localStorage.getItem(this.key) as string;
            
            if(!compiledTokens) this.cached = null;
            else this.cached = new Credentials(compiledTokens);
        }

        return this.cached;
    }
    
    public save(credentials: Credentials): boolean {
        this.cached = credentials;
        localStorage.setItem(this.key, this.cached.compile());

        return true;
    }
    
    public revoke(): boolean {
        this.cached = null;
        localStorage.removeItem(this.key);
        
        return true;
    }

}