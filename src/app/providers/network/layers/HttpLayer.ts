/**
 * Classic http network layer.
 */
export class HttpLayer {
    constructor(protected uri: string) {};

    /**
     * Classic POST request.
     * @param options Options and configuration of the request
     */
    public async post({ method, data, headers }: { method: string, data?: Object, headers?: any }): Promise<any> {
        let url = this.uri + method;

        headers = headers || [];
        data = data || {};

        headers['content-type'] = "application/json";

        let response = await fetch(url, {
            body: JSON.stringify(data),
            cache: 'no-cache',
            credentials: 'same-origin',
            headers,
            method: 'POST',
            mode: 'cors',
            redirect: 'follow',
            referrer: 'no-referrer'
        });
        if (!response.ok) {
            throw await response.json();
        }
        return await response.json();
    };

    /**
     * Classic GET request.
     * @param options Options and configuration of the request
     */
    public async get({ method, headers }: { method: string, headers?: any }): Promise<any> {
        let url = this.uri + method;
        headers = headers || [];

        headers['content-type'] = "application/json";

        let response = await fetch(url, {
            cache: 'no-cache',
            credentials: 'same-origin',
            headers,
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
            referrer: 'no-referrer'
        });
        if (!response.ok) {
            throw await response.json();
        }
        return await response.json();
    };

    /**
     * Classic REST request. The layer will decide if it will make a POST or a GET based on your options.
     * @param options Options and configuration of the request
     */
    public async call({ method, data, headers }: { method: string, data?: Object, headers?: any }): Promise<any> {
        headers = headers || [];
    
        if (typeof data == "undefined" || data == null)
            return this.get({ method, headers });
        return this.post({ method, data, headers });
    };
};