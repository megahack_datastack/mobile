import ApolloClient from "apollo-client";
import { split, ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { Context } from '../context/Context';

/**
 * Network layer responsible for graphql queries interaction
 */
export class GraphLayer {
    private httpLink: HttpLink;
    private wsLink: WebSocketLink;
    private splitedLink: ApolloLink;
    private _client: ApolloClient<NormalizedCacheObject>;
    private context: Context;

    constructor(protected uri: string, protected socketsUri, protected graphUri: string, protected subscriptionsUri: string) { };

    private provideConnectionParameters() {
        return this.context.export();
    };

    /**
     * Changes the current context.
     * @param context The alternative context
     */
    public swapContext(context: Context) {
        this.context = context;
    };

    /**
     * Initialize the layer using a start context (providing the auth headers for the ws connection)
     * @param context The initialization context
     */
    public init(context: Context) {
        this.swapContext(context);

        this.httpLink = new HttpLink({ uri: this.uri + this.graphUri });
        this.wsLink = new WebSocketLink({
            uri: this.socketsUri.replace(/http/g, "ws") + this.subscriptionsUri, options: {
                reconnect: true,
                connectionParams: this.provideConnectionParameters.bind(this)
            }
        });
        this.splitedLink = split(
            ({ query }) => {
                const { kind, operation } = <any>getMainDefinition(query);
                return kind === 'OperationDefinition' && operation === 'subscription';
            },
            this.wsLink,
            this.httpLink
        );
        this._client = new ApolloClient({
            link: this.splitedLink,
            cache: new InMemoryCache(),
            defaultOptions: {
                query: {
                    fetchPolicy: 'network-only',
                    errorPolicy: 'all',
                },
            }
        });
    };

    /**
     * Export the final client.
     * @returns GraphQL client
     */
    public client(): ApolloClient<NormalizedCacheObject> {
        return this._client;
    }
}