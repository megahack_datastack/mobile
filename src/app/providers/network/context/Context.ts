import { Credentials } from '../auth/Credentials';
/**
 * Class used as a context container
 */
export class Context {
    private static ACCESS_TOKEN_KEY: string = 'x-access-token';
    private static REFRESH_TOKEN_KEY: string = 'x-refresh-token';
    private static RESOURCE_KEY_PREFIX: string = 'x-resource-';

    private credentials: Credentials;
    private headers: {[key:string]: string} = { };

    public constructor(credentials: Credentials = null) {
        this.authorize(credentials);
    }

    /**
     * Compile and export the generated stores into a headers collection
     * @returns The compiled headers collection
     */
    public export(): {[key: string]: string} {
        return this.headers;
    }

    /**
     * Sets a value inside the headers store for later usage.
     * @param key The key in the headers store.
     * @param value The value that will be set.
     * @returns The context (for chaining)
     */
    public header(key: string, value: string): Context {
        if(!value) delete this.headers[key];
        else this.headers[key] = value;

        return this;
    }

    /**
     * Authorize the current context agains a set of credentials.
     * @param credentials Set of credentials used in the authorization process
     * @returns The context (for chaining)
     */
    public authorize(credentials: Credentials): Context {
        if(!credentials) return this;

        if(!!credentials) this.credentials = credentials;

        this.header(Context.ACCESS_TOKEN_KEY, this.credentials.tokens.access);
        this.header(Context.REFRESH_TOKEN_KEY, this.credentials.tokens.refresh);
    
        return this;
    }

    /**
     * Checks is the context is authorized.
     * 
     * @returns Authorization state
     */
    public isAuthorized(): boolean {
        return !!this.credentials;
    }

    /**
     * Sets a value inside the resource store for later usage.
     * @param name The name of the resource
     * @param value The value of the resource
     * @returns The context (for chaining)
     */
    public resource(name: string, value: string): Context {
        if(!!name) this.header(`${Context.RESOURCE_KEY_PREFIX}${name}`, value);

        return this;
    }

}