import { Context } from "../context/Context";
import { HttpLayer } from '../layers/HttpLayer';
import { GraphLayer } from '../layers/GraphLayer';
import { DocumentNode } from "graphql";
import { FetchResult, Observable } from "apollo-link";

/**
 * Class responsible for dispatching requests to the network layers.
 */
export class Executor {
    public constructor(private context: Context, private http: HttpLayer, private graph: GraphLayer) { };

    /**
     * Makes a classic GET request
     * @param method API method to call
     * @returns Promise to the result
     */
    public async get(method: string): Promise<any> {
        return await this.http.get({ method, headers: this.context.export() });
    };

    /**
     * Makes a classic POST request
     * @param method API method to call
     * @param data Data for the body
     * @returns Promise to the result
     */
    public async post(method: string, data: any = {}): Promise<any> {
        return await this.http.post({ method, data, headers: this.context.export() });
    };

    /**
     * Makes a classic REST request. The network layer will decide if it will be a GET or a POST.
     * @param method API method to call
     * @returns Promise to the result
     */
    public async call(method: string, data: any = null): Promise<any> {
        return await this.http.call({ method, data, headers: this.context.export() });
    };

    /**
     * Makes a query to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    public async query<T = any>(query: DocumentNode, options?: any): Promise<FetchResult<T>> {
        return await this.graph.client().query<T>({ query, context: { headers: this.context.export() }, ...options });
    };

    /**
     * Makes a mutation to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    public async mutate<T = any>(mutation: DocumentNode, options?: any): Promise<FetchResult<T>> {
        return await this.graph.client().mutate<T>({ mutation, context: { headers: this.context.export() }, ...options });
    };

    /**
     * Makes a subscription to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    public async subscribe<T = any>(query: DocumentNode, options?: any): Promise<Observable<T>> {
        return this.graph.client().subscribe<T>({ query, ...options });
    };
};