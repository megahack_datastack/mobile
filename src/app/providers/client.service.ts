import { Injectable } from '@angular/core';
import { ICredentialsProvider } from './network/auth/ICredentialsProvider';
import { LocalStorageCredentialsProvider } from './network/auth/LocalStorageCredentialsProvider';
import { ContextProvider } from './network/context/ContextProvider';
import { HttpLayer } from './network/layers/HttpLayer';
import { GraphLayer } from './network/layers/GraphLayer';
import { FetchResult, Observable } from 'apollo-link';
import { Context } from './network/context/Context';
import { Executor } from './network/execution/Executor';
import { environment } from '../../environments/environment';
import * as _ from "lodash";
import { Router } from '@angular/router';
import { ToastsService } from './toasts.service';

@Injectable()
export class ClientService {
  public credentialsProvider: ICredentialsProvider;
  public contextProvider: ContextProvider;
  public http: HttpLayer;
  public graph: GraphLayer;

  public constructor(
    private router: Router,
    private toasts: ToastsService
  ) {
    this.credentialsProvider = new LocalStorageCredentialsProvider(environment.api.tokenStorageKey);
    this.contextProvider = new ContextProvider(this.credentialsProvider);
    
    this.http = new HttpLayer(environment.api.url);
    this.graph = new GraphLayer(environment.api.url, environment.api.sockets, 'graph', 'graph/subscriptions');
    this.graph.init(this.contextProvider.exportOptimalContext());
  };

  public updateContext() {
    this.graph.swapContext(this.contextProvider.exportOptimalContext());
  };

  public isAuthorized(): boolean {
    return this.contextProvider.exportOptimalContext().isAuthorized();
  };

  private copy(obj: any): any {
    return _.cloneDeep(obj);
  };

  public async scope<T = any>(executorFunction: (executor: Executor) => Promise<FetchResult<T>>, contextFunction: (context: Context) => Context | Promise<Context> = (x) => x): Promise<FetchResult<T>> {
    let ctx = this.contextProvider.exportOptimalContext();
    ctx = await contextFunction(ctx);

    let executor = new Executor(ctx, this.http, this.graph);
    let result;
    try {
      result = await executorFunction(executor);
    } catch(ex) {
      let { networkError } = ex;
      if(networkError && networkError.statusCode == 400 && networkError.result.name == "InvalidTokenError") {
        this.toasts.createToast('error', 'Could not connect you to our services! Please try and login again.');
        this.credentialsProvider.revoke();
        this.router.navigate(['/login']);
      } else {
        this.toasts.createToast('error', ex.message);
      }
    }
    executor = null;
    
    return this.copy(result);
  };

  public async subscription<T = any>(executorFunction: (executor: Executor) => Promise<Observable<T>>, contextFunction: (context: Context) => Context | Promise<Context> = (x) => x): Promise<Observable<T>> {
    let ctx = this.contextProvider.exportOptimalContext();
    ctx = await contextFunction(ctx);

    let executor = new Executor(ctx, this.http, this.graph);
    let result;
    try {
      result = await executorFunction(executor);
    } catch(ex) {
      let { networkError } = ex;
      if(networkError && networkError.statusCode == 400 && networkError.result.name == "InvalidTokenError") {
        this.toasts.createToast('error', 'Could not connect you to our services! Please try and login again.');
        this.credentialsProvider.revoke();
        this.router.navigate(['/login']);
      } else {
        this.toasts.createToast('error', ex.message);
      }
    }
    executor = null;
    
    return this.copy(result);
  };
};
