import { Injectable } from '@angular/core';
import { ClientService } from "./client.service";
import { ToastsService } from './toasts.service';
import { FetchResult } from 'apollo-link';
import { Credentials } from './network/auth/Credentials';

@Injectable()
export class LoginService {

  constructor(
    private client: ClientService,
    private toasts: ToastsService
  ) {

  }

  public redirectUrl = '';

  public async signUp(body: { email: string, password: string, confirm: string }): Promise<any> {
    try {
      body.password = btoa(body.password);
      body.confirm = btoa(body.confirm);

      let { data } = await this.client.scope(request => request.call('auth/register', body)) as FetchResult;

      let credentials = new Credentials(`${data.token}|${data.refresh}`);
      this.client.credentialsProvider.save(credentials);
      this.client.updateContext();

      return {
        valid: true,
        url: this.redirectUrl ? this.redirectUrl : '/dashboard'
      };
    } catch (ex) {
      return {
        valid: false,
      };
    }
  }

  public async logIn(body: { email: string, password: string }): Promise<any> {
    try {
      body.password = btoa(body.password);

      let { data } = await this.client.scope(request => request.call('auth/login', body)) as FetchResult;
      let credentials = new Credentials(`${data.token}|${data.refresh}`);
      this.client.credentialsProvider.save(credentials);
      this.client.updateContext();

      return {
        valid: true,
        url: this.redirectUrl ? this.redirectUrl : '/dashboard',
        hasCompletedRegistration: data.hasCompletedRegistration
      };
    } catch (ex) {
      this.client.credentialsProvider.revoke();
      this.client.updateContext();

      return {
        valid: false,
      };
    }
  }
}
