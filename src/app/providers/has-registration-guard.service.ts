import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  CanLoad, Route
}                           from '@angular/router';
import { ApiService } from './api.service';

@Injectable()
export class HasRegistrationGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private api: ApiService, private router: Router) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let url: string = state.url;

    return await this.checkLogin(url);
  }

  async canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return await this.canActivate(route, state);
  }

  async canLoad(route: Route): Promise<boolean> {
    let url = `/${route.path}`;

    return await this.checkLogin(url);
  }

  async checkLogin(url: string): Promise<boolean> {
    let data = await this.api.me();
    if(!data) { 
        return false;
    }

    if(!(data.me!.hasCompletedRegistration)) {
        this.router.navigate(['identity']);
        return false;
    }

    return true;
  }
}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/