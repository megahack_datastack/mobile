import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../providers/api.service';
import { LoginService } from '../providers/login.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html'
})
export class LogInComponent implements OnInit {

  constructor(private router: Router, private loginService: LoginService) { }

  isLoading = false;

  user = {
    email: '',
    password: ''
  };

  ngOnInit() {
  }

  async login (form)  {
    this.isLoading = true;
    let data = await this.loginService.logIn(this.user);

    setTimeout(() => {
      if(data.valid) {
        if(data.hasCompletedRegistration) {
          this.router.navigate(['dashboard']);
        } else {
          this.router.navigate(['identity'])
        }
      } else {
        form.reset({ email: '', password: '' });
      }
      this.isLoading = false;
    }, 2000);
  }

}
