import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OcrServiceProvider } from '../providers/ocr/ocr-service';
import { Base64 } from "@ionic-native/base64/ngx";

import * as moment from "moment";
import { Document } from '../providers/ocr/types/Document';
import { ClientService } from '../providers/client.service';
import { ToastsService } from '../providers/toasts.service';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-who-are-you',
  templateUrl: './who-are-you.component.html'
})
export class WhoAreYouComponent implements OnInit {

  constructor(private router: Router, private ocr: OcrServiceProvider, private base64: Base64, private client: ClientService, private api: ApiService, private toasts: ToastsService) { }

  showPageLoader = false;
  isLoading = false;
  step = 'scan';

  user = {
    firstName: '',
    lastName: '',
    idNumber: '',
    uniqueidentifier: '',
    citizenship: '',
    birthday: ''
  };

  ngOnInit() {
  }

  private result: Document;

  async scanId() {
    this.step = 'scan';
    this.result = this.ocr.process(await this.ocr.scan());
    this.showPageLoader = true;

    setTimeout(() => {
      this.showPageLoader = false;
      this.step = 'confirm';

      this.user = {
        firstName: this.result.givenNames,
        lastName: this.result.surNames,
        idNumber: this.result.documentNumber,
        uniqueidentifier: [this.result.personalNumber.slice(0, 1), this.result.dayOfBirth, this.result.personalNumber.slice(1)].join(''),
        citizenship: this.result.countryCode,
        birthday: moment(this.result.dayOfBirthObject).format("DD/MM/YYYY")
      };
    }, 3000);
  }

  async sendIdentity() {
    this.showPageLoader = true;

    let res = await this.base64.encodeFile(this.result.imagePath);
    let _data;
    try {
      let { data } = await this.client.scope(executor => executor.post('upload', { imageData: res, extension: 'jpg' }));
      _data = data;
    } catch (ex) {
      console.error(ex);
      this.toasts.createToast('error', 'Woops! There seems to be a problem! You may want to try again!');
      this.step = 'scan';
      this.showPageLoader = false;
      return;
    }

    try {
      if (!_data) _data = {};
      let uploaded = _data.id;

      let d = { ...this.result, uploaded };
      if (!(await this.api.document(d))) {
        this.toasts.createToast('error', 'Woops! There seems to be a problem! You may want to try again!');
        this.step = 'scan';
        this.showPageLoader = false;
      }
    } catch (ex) {
      this.toasts.createToast('error', 'Woops! There seems to be a problem! You may want to try again!');
      this.step = 'scan';
      this.showPageLoader = false;
    }

    setTimeout(() => {
      this.router.navigate(['/dashboard']);
    }, 2000);
  }
}
