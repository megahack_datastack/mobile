import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.scss']
})
export class NewContractComponent implements OnInit {

  constructor(private router: Router, private scanner: BarcodeScanner) { }

  showPageLoader = false;
  isLoading = false;

  ngOnInit() {
  }

  async scanQr() {
    let data = await this.scanner.scan();
    this.showPageLoader = true;
    if(data) {
      if(!data.cancelled) {
        if(!!data.text) {
          setTimeout(() => {
              this.showPageLoader = false;
              this.router.navigate([`contract/${data.text}`]);
            }, 4000);
        }
      }
    }
  }

}
