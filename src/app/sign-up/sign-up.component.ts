import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../providers/login.service';
import { ToastsService } from '../providers/toasts.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent implements OnInit {

  constructor(private router: Router, private loginService: LoginService, private toasts: ToastsService) { }

  isLoading = false;

  user = {
    email: '',
    password: '',
    confirm: ''
  };

  ngOnInit() {
  }

  async signIn (form) {
    this.isLoading = true;
    let data = await this.loginService.signUp(this.user);
    if(!data) {
      this.toasts.createToast('error', 'Woops! Something went wrong! We\'re sorry :(');
    }

    setTimeout(() => {
      if(data.valid) {
        this.router.navigate(['identity']);
      } else {
        form.reset({email: '', password: '', confirm: ''});
      }
      this.isLoading = false;
    }, 2000);
  }
}
