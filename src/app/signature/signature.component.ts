import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../providers/client.service';
import { ToastsService } from '../providers/toasts.service';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss']
})
export class SignatureComponent implements AfterViewInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private image = "";

  showPageLoader = false;

  private signaturePadOptions: Object = {
    'minWidth': 5,
    'canvasWidth': 500,
    'canvasHeight': 300,
    'backgroundColor': '#fff'
  };
 
  constructor(private route: ActivatedRoute, private client: ClientService, private api: ApiService, private toasts: ToastsService, private router: Router) {
  }
 
  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 5);
    this.signaturePad.clear();
  }
 
  drawStart() { }
  drawComplete() {
    this.image = this.signaturePad.toDataURL();
  }

  clear() {
    this.signaturePad.clear();
  }

  async send() {
    this.showPageLoader = true;
    let { contract, authorization } = this.route.snapshot.params;
    let { data } = await this.client.scope(executor => executor.post('upload', { imageData: this.image, extension: 'png' }));
    if(!data || !data.id) {
      this.toasts.createToast('error', 'Woooops! Something went wrong...');
      this.showPageLoader = false;
      this.signaturePad.clear();
      return;
    }
    let result = await this.api.sign(contract, authorization, data.id);
    if(!result) {
      this.toasts.createToast('error', 'Wooops! Something went wrong! :(');
    }
    
    let r = Math.random().toString(36).substr(0, 5);
    this.router.navigate([`dashboard/${r}`]);

    this.showPageLoader = false;
  }
}
