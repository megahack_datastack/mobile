import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { ApiService } from '../providers/api.service';
import { ToastsService } from '../providers/toasts.service';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ClientService } from '../providers/client.service';

import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-contract-details',
  templateUrl: './contract-details.component.html',
  styleUrls: ['./contract-details.component.scss']
})
export class ContractDetailsComponent implements OnInit {

  constructor(
    public route: ActivatedRoute, private api: ApiService, private client: ClientService, private router: Router, private toasts: ToastsService, private camera: Camera,
    private file: File, private fileTransfer: FileTransfer, private document: DocumentViewer, private platform: Platform
  ) { }

  showPageLoader = true;
  signedContract: any = {};
  isSigned: boolean = false;
  showModal: boolean = false;

  contract = {
    name: "New Client Contract",
    provider: {
      name: "Vodafone"
    },
    description: "Id laborum ullamco quis irure do sunt dolor in pariatur esse. Magna sunt excepteur duis sunt deserunt reprehenderit mollit exercitation sint consequat duis aliqua consequat irure. Officia minim anim occaecat eiusmod mollit elit nulla veniam irure cillum in. Culpa et sunt exercitation in occaecat anim sunt in dolore sint. Tempor occaecat non consequat culpa amet aliquip.\n\nAmet id laborum minim do commodo Lorem Lorem. Anim veniam mollit culpa sint incididunt cupidatat in exercitation aute. Nulla sit pariatur dolor quis Lorem aliqua ad incididunt esse fugiat deserunt consequat qui. Excepteur dolore incididunt tempor eiusmod eiusmod amet consectetur laborum duis dolore deserunt. Incididunt nisi fugiat veniam adipisicing voluptate laboris pariatur pariatur ullamco quis ex occaecat incididunt exercitation. Laboris duis quis ad labore ea enim proident occaecat cupidatat ad aliqua ea adipisicing. Ut voluptate ipsum eu nostrud ullamco mollit magna officia ex proident non esse.\n\nExercitation excepteur officia pariatur ipsum veniam adipisicing dolore tempor laborum eu sint culpa. Consequat irure laboris dolore anim incididunt. Mollit cupidatat voluptate fugiat labore esse ea culpa et incididunt id. Dolore quis laboris et enim anim pariatur Lorem sint tempor. Eu amet proident mollit cillum laborum aute elit veniam mollit commodo fugiat eiusmod fugiat nulla.",
    preview: "./assets/images/contract.png",
    uploaded: "https://19of32x2yl33s8o4xza0gf14-wpengine.netdna-ssl.com/wp-content/uploads/Exhibit-A-SAMPLE-CONTRACT.pdf"
  }

  private id: string = "";

  async action() {
    if(!this.signedContract.signature) {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        cameraDirection: 1, // front
        correctOrientation: true
      };
      let image = await this.camera.getPicture(options) as string;
      this.showPageLoader = true;
      let { data } = await this.client.scope(executor => executor.post('upload', { imageData: image, extension: 'jpeg' }));
      if(!!data && !!data.id) {
        let res = await this.api.authorize(data.id);
        if(!res.authorized) { 
          setTimeout(_ => {
            this.showPageLoader = false;
            this.toasts.createToast('error', 'Oh-oh! No face match! :( Try again!');
          }, 2000);
        } else {
          this.router.navigate([`signature/${this.id}/${res.id}`]);
        }
      } else {
        setTimeout(_ => {
          this.showPageLoader = false;
        }, 2000);
      }
    } else {
      this.showModal = true;
    }
  }

  async ngOnInit() {
    let id = this.route.snapshot.params["id"];
    this.id = id;

    try {
      let data = await this.api.contract(id);
      this.signedContract = data.signedContract;
      this.signedContract.contract.preview = `${environment.api.url}upload/${data.signedContract.contract.preview}`;
      this.signedContract.contract.uploaded = `${environment.api.url}upload/${data.signedContract.contract.uploaded}`;

      this.contract = this.signedContract.contract;

      if(!!this.signedContract.qr && !!this.signedContract.signature) {
        this.isSigned = true;
      }

      setTimeout(_ => {
        this.showPageLoader = false;
      }, 2000);
    } catch(ex) {
      console.log(ex);
      this.toasts.createToast('error', 'Wooops! Looks like something went wrong..');
      this.router.navigate(['new-contract']);
    }
  }

  closeModal () {
    this.showModal = false;
  }

  showDoc() {
    let path = null;
 
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }
 
    const transfer = this.fileTransfer.create();
    transfer.download(this.contract.uploaded, path + 'file.pdf').then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
    });
  }
}
