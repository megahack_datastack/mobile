import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WhoAreYouComponent } from './who-are-you/who-are-you.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewContractComponent } from './new-contract/new-contract.component';
import { ContractDetailsComponent } from './contract-details/contract-details.component';
import { AuthGuard } from './providers/auth-guard.service';
import { LoginAuthGuard } from './providers/login-auth-guard';
import { HasRegistrationGuard } from './providers/has-registration-guard.service';
import { SignatureComponent } from './signature/signature.component';

const routes: Routes = [
  {
    path: '',
    component: LogInComponent,
    canActivate: [ LoginAuthGuard ],
    canActivateChild: [ LoginAuthGuard ]
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    canActivate: [ LoginAuthGuard ],
    canActivateChild: [ LoginAuthGuard ]
  },
  {
    path: 'identity',
    component: WhoAreYouComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  },
  { path: 'dashboard', redirectTo: 'dashboard/' },
  {
    path: 'dashboard/:r',
    component: DashboardComponent,
    canActivate: [ AuthGuard, HasRegistrationGuard ],
    canActivateChild: [ AuthGuard, HasRegistrationGuard ]
  },
  {
    path: 'new-contract',
    component: NewContractComponent,
    canActivate: [ AuthGuard, HasRegistrationGuard ],
    canActivateChild: [ AuthGuard, HasRegistrationGuard ]
  },
  {
    path: 'contract/:id',
    component: ContractDetailsComponent,
    canActivate: [ AuthGuard, HasRegistrationGuard ],
    canActivateChild: [ AuthGuard, HasRegistrationGuard ]
  },
  {
    path: 'signature/:contract/:authorization',
    component: SignatureComponent,
    canActivate: [ AuthGuard, HasRegistrationGuard ],
    canActivateChild: [ AuthGuard, HasRegistrationGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
