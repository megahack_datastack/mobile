import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filterContracts' })
export class SearchContracts implements PipeTransform {
  transform(contracts, searchText: string) {
    let filteredContracts = [];

    for (let i = 0; i < contracts.length; i++) {
        if (contracts[i].name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1 || contracts[i].provider.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1) {
            filteredContracts.push(contracts[i]);
        }
    }
    return filteredContracts;
  }
}