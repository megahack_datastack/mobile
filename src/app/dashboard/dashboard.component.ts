import { Component, OnInit } from '@angular/core';
import { ApiService } from '../providers/api.service';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ClientService } from '../providers/client.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private api: ApiService, private router: Router, private route: ActivatedRoute, private client: ClientService) { }

  showPageLoader = false;
  isLoading = false;
  searchContracts = '';
  contracts = [ ];

  async ngOnInit() {
    this.route.params.subscribe(async _ => {
      this.contracts = await this.api.contracts();
    });
  }

  open(contract) {
    this.router.navigate([`contract/${contract.id}`]);
  }

  logOut() {
    this.client.credentialsProvider.revoke();
    this.router.navigate(['']);
  }
}
