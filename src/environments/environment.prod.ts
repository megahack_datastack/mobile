export const environment = {
  production: true,
  api: {
    url: 'http://192.168.43.115:4100/api/',
    sockets: 'http://192.168.43.115:4100/api/',
    tokenStorageKey: 'atk'
  }
};
